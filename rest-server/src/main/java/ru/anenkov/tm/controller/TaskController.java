package ru.anenkov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.anenkov.tm.enumerated.Status;
import ru.anenkov.tm.model.AbstractEntity;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @ModelAttribute("status")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/tasks")
    public String index(Model model) {
        List<Task> tasks = taskService.getList();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.add(new Task("new Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskService.removeOneById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        taskService.add(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Task task = taskService.findOneByIdEntity(id);
        List<Project> projects = projectService.getList();
        model.addAttribute("task", task);
        model.addAttribute("projectList", projects);
        return "task-edit";
    }

}
