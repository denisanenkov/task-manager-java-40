package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;

import java.util.List;

@RequestMapping("/api")
public interface IProjectRestEndpoint {

    @PostMapping("/project")
    Project add(@Nullable Project project);

    @PutMapping("/project")
    Project update(@Nullable Project project);

    @GetMapping("/project/{id}")
    @Nullable ProjectDTO findOneByIdEntity(@PathVariable("id") String id);

    @DeleteMapping("/project/{id}")
    String removeOneById(@PathVariable String id);

    @DeleteMapping("/projects")
    void removeAllProjects();

    @Nullable
    @GetMapping("/projects")
    List<ProjectDTO> getList();

    @GetMapping("/projects/count")
    long count();

}
