package ru.anenkov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api")
public interface ITaskRestEndpoint {

    @GetMapping("/{projectId}/tasks")
    List<TaskDTO>tasks(@PathVariable String projectId);

    @GetMapping("/{projectId}/task/{id}")
    TaskDTO getTask(@PathVariable String projectId, @PathVariable String id);

    @PostMapping("/task")
    TaskDTO addTask(@RequestBody Task task);

    @PutMapping("/task")
    TaskDTO updateTask(@RequestBody Task task);

    @DeleteMapping("/{projectId}/task/{id}")
    String deleteTask(@PathVariable String projectId, @PathVariable String id);

    @DeleteMapping("/{projectId}/tasks")
    void deleteAllTasks(@PathVariable String projectId);

    @GetMapping("/{projectId}/tasks/count")
    long count(@PathVariable String projectId);

}
