package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("Error! User is Empty..");
    }
}
