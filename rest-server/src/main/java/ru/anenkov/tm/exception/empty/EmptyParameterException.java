package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyParameterException extends AbstractException {

    public EmptyParameterException() {
        System.out.println("Error! Empty name parameter exception..");
    }

}
