package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component; 
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.service.ProjectService;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @WebMethod
    public void add(@WebParam(name = "project") Project project) {
        projectService.add(project);
    }

    @WebMethod
    public ProjectDTO findOneByIdEntity(@WebParam(name = "id") String id) {
        return projectService.findOneByIdEntity(id);
    }

    @WebMethod
    public void removeOneById(@WebParam(name = "id") String id) {
        projectService.removeOneById(id);
    }

    @WebMethod
    public void removeAllProjects() {
        projectService.removeAllProjects();
    }

    @WebMethod
    public List<ProjectDTO> getList() {
        return ProjectDTO.toProjectListDTO(projectService.getList());
    }

    @WebMethod
    public long count() {
        return projectService.count();
    }

}
