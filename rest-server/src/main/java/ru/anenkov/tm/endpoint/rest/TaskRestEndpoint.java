package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.api.endpoint.ITaskRestEndpoint;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestEndpoint implements ITaskRestEndpoint { 

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping("/{projectId}/tasks")
    public List<TaskDTO> tasks(@PathVariable String projectId) {
        return TaskDTO.toTaskListDTO(taskService.findAllByProjectId(projectId));
    }

    @Override
    @GetMapping("/{projectId}/task/{id}")
    public TaskDTO getTask(@PathVariable String projectId, @PathVariable String id) {
        TaskDTO task = TaskDTO.toTaskDTO(taskService.findTaskByIdAndProjectId(id, projectId));
        System.out.println("FINDED: " + task);
        if (task == null) throw new NoSuchEntitiesException
                ("Entity \"Task\" with id = " + id + " not found!");
        return task;
    }

    @Override
    @PostMapping("/task")
    public TaskDTO addTask(@RequestBody Task task) {
        taskService.add(task);
        return TaskDTO.toTaskDTO(task);
    }

    @Override
    @PutMapping("/task")
    public TaskDTO updateTask(@RequestBody Task task) {
        taskService.add(task);
        return TaskDTO.toTaskDTO(task);
    }

    @Override
    @DeleteMapping("/{projectId}/task/{id}")
    public String deleteTask(@PathVariable String projectId, @PathVariable String id) {
        taskService.removeTaskByIdAndProjectId(id, projectId);
        return "Task with id " + id + " was deleted successfully!";
    }

    @Override
    @DeleteMapping("/{projectId}/tasks")
    public void deleteAllTasks(@PathVariable String projectId) {
        taskService.removeAllByProjectId(projectId);
    }

    @Override
    @GetMapping("/{projectId}/tasks/count")
    public long count(@PathVariable String projectId) {
        return taskService.countByProjectId(projectId);
    }

}
