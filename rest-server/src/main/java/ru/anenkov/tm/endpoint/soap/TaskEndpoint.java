package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.TaskService;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @WebMethod
    public void add(@WebParam(name = "task") Task task) {
        taskService.add(task);
    }

    @WebMethod
    public void removeAllByProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        taskService.removeAllByProjectId(projectId);
    }

    @WebMethod
    public List<TaskDTO> findAllByProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return TaskDTO.toTaskListDTO(taskService.findAllByProjectId(projectId));
    }

    @WebMethod
    public void removeTaskDTOByIdAndProjectId(
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        taskService.removeTaskByIdAndProjectId(id, projectId);
    }

    @WebMethod
    public TaskDTO findTaskDTOByIdAndProjectId(
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByIdAndProjectId(id, projectId));
    }

    @WebMethod
    public long countByProjectId(
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return taskService.countByProjectId(projectId);
    }

}
