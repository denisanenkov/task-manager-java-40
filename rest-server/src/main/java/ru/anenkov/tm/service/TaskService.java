package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @SneakyThrows
    public Task toTask(@Nullable final Optional<Task> taskDTO) {
        @Nullable Task newTask = new Task();
        newTask.setId(taskDTO.get().getId());
        newTask.setName(taskDTO.get().getName());
        newTask.setDescription(taskDTO.get().getDescription());
        newTask.setDateBegin(taskDTO.get().getDateBegin());
        newTask.setDateFinish(taskDTO.get().getDateFinish());
        newTask.setStatus(taskDTO.get().getStatus());
        newTask.setProject(taskDTO.get().getProject());
        return newTask;
    }

    @SneakyThrows
    public List<Task> toTaskList(@Nullable final List<Optional<Task>> taskOptDtoList) {
        List<Task> taskList = new ArrayList<>();
        for (Optional<Task> task : taskOptDtoList) {
            taskList.add(toTask(task));
        }
        return taskList;
    }

    @Override
    public void removeAllByProjectId(@Nullable String projectId) {
        taskRepository.removeAllByProjectId(projectId);
    }

    @Override
    public List<Task> findAllByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task findTaskByIdAndProjectId(@Nullable String id, @Nullable String projectId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.findTaskByIdAndProjectId(id, projectId);
    }

    @Override
    public void removeTaskByIdAndProjectId(@Nullable String id, @Nullable String projectId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        taskRepository.removeTaskByIdAndProjectId(id, projectId);
    }

    @Override
    public long countByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.countByProjectId(projectId);
    }

    @Override
    public Task save(@Nullable Task task) {
        if (task == null) throw new EmptyEntityException();
        return taskRepository.save(task);
    }

    public void add(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    public Task findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toTask(taskRepository.findById(id));
    }

    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    public List<Task> getList() { return taskRepository.findAll(); }

    public void removeAllTasks() { taskRepository.deleteAll(); }

    public long count() { return taskRepository.count(); }

}
