package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @SneakyThrows
    public Project toProject(@Nullable final Optional<Project> projectDTO) {
        @Nullable Project newProject = new Project();
        newProject.setId(projectDTO.get().getId());
        newProject.setName(projectDTO.get().getName());
        newProject.setDescription(projectDTO.get().getDescription());
        newProject.setDateBegin(projectDTO.get().getDateBegin());
        newProject.setDateFinish(projectDTO.get().getDateFinish());
        newProject.setStatus(projectDTO.get().getStatus()); 
        return newProject;
    }

    @SneakyThrows
    public List<Project> toProjectList(@Nullable final List<Optional<Project>> projectOptDtoList) {
        List<Project> projectList = new ArrayList<>();
        for (Optional<Project> project : projectOptDtoList) {
            projectList.add(toProject(project));
        }
        return projectList;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(
            @Nullable final Project project
    ) {
        if (project == null) throw new EmptyEntityException();
        projectRepository.save(project);
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public ProjectDTO findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ProjectDTO.toProjectDTO(projectRepository.getOne(id));
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    @Override
    public void removeAllProjects() {
        projectRepository.deleteAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Project> getList() {
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public long count() {
        return projectRepository.count();
    }

}
