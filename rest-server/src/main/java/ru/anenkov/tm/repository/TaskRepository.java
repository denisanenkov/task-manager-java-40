package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Transactional
    void removeAllByProjectId(@Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    List<Task> findAllByProjectId(@Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    Task findTaskByIdAndProjectId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeTaskByIdAndProjectId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    long countByProjectId(@Nullable @Param("projectId") final String projectId);

}
